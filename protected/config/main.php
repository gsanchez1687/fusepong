<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Control Tickets',
	//'defaultController' => '/login',
	'language' => 'es',
	'sourceLanguage' => 'es',
	'theme' => 'classic',
	'charset' => 'utf-8',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		'usuarios',
		'empresas',
		'historia',
		'proyecto',
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'16871752',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'showScriptName' => false,
			'caseSensitive' => true,
			'urlFormat' => 'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				//RUTAS PERSONALIZADAS PRA USUARIOS
				'login'=>'/usuarios/default/login',
				'logout'=>'/usuarios/default/logout',
				'registro'=>'/usuarios/default/registrar',
				'perfil'=>'/usuarios/default/perfil',
				//RUTAS PERSONALIZADAS PARA EMPRESAS O COMPAÑIAS
				'empresalista'=>'/empresas/empresas/admin',
				'empresacrear'=>'/empresas/empresas/create',
				//RUTAS PERSONALOZADAS PARA HISTORIAS
				'historialista'=>'/historia/historias/admin',
				'historiacrear'=>'/historia/historias/create',
				//RUTAS PERSONALIZADAS PARA PROCTOS
				'proyectolista'=>'/proyecto/proyectos/admin',
				'proyectocrear'=>'/proyecto/proyectos/create',
			),
		),
		

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail' => 'gsanchez1687@gmail.com',
		'adminEmail'=>'webmaster@example.com',
		'view-text' => 'Detalle',
		'view-icon' => '<i class="fa fa-eye"></i>',
		'view-btn' => 'btn btn-dark',
		'update-text' => 'Editar',
		'update-icon' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
		'update-btn' => 'btn btn-dark',
		'delete-text' => 'Quitar',
		'delete-icon' => '<i class="fa fa-trash"></i>',
		'delete-btn'=>'btn btn-dark',
	),
);
