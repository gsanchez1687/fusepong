<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;   

    public function authenticate() {
            
        $user = Usuarios::model()->find(
			array(
					'condition' => "t.correo = :correo OR t.username =:username AND password=:password",
					'params' => array(':correo' => strtolower($this->username),':username'=>strtolower($this->username), ':password'=>sha1($this->password))   
				)
        );

        $UsuariosEmrpesas = UsuariosEmpresas::model()->find('usuario_id = ?',array( $user->id ));

        if ($user === null){
            
            Yii::app()->user->setFlash('success', "La cuenta a la que intentas ingresar no existe");
        }
        
        else if (!$user->validatePassword($this->password)){
           Yii::app()->user->setFlash('success', "La contraseña es incorrecta");
           
        } else if($user->estado == '9'){
            
            Yii::app() -> user->setFlash('success', "Usuario Inactivo");
	    $this->errorCode = self::ERROR_PASSWORD_INVALID;
            
        }  else if($user->estado == '10' ) {
            
            Yii::app() -> user->setFlash('success', "Usuario Suspendido");
            $this ->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        
        else {
            $this->_id = $user->id;
            $this->setState('username', $user->username);
            $this->setState('perfil_id', $user->perfil_id);
            $this->setState('nombre', $user->nombres);
            $this->setState('apellido', $user->apellidos);
            $this->setState('empresa_id', $UsuariosEmrpesas->empresa_id);
            $this->username = $user->username;
           
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
    }
    
    public function getId() {
        return $this->_id;
    }

}
