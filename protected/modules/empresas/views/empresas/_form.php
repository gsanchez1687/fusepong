<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'empresas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-danger')); ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'nombre',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>	
		<br><br>
		<div class="form-group">
			<?php echo $form->labelEx($model,'nit',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'nit',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>	
		<br><br>
		<div class="form-group">
			<?php echo $form->labelEx($model,'telefono',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'telefono',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>
		<br><br>
		<div class="form-group">
			<?php echo $form->labelEx($model,'direccion',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>
		<br><br>
		<div class="form-group">
			<?php echo $form->labelEx($model,'correo',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'correo',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>
		<br><br>
		<div class="form-group">
			<?php echo $form->labelEx($model,'url',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			</div>
		</div>

	
	<br><br>
	<div class="buttons">
		<a href="<?php echo Yii::app()->createUrl('empresalista'); ?>" class="btn btn-success">Volver</a>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear Empresa' : 'Actualizar Empresa',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->