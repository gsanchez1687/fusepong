<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Lista de empresas</h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('historiacrear'); ?>" >Crear Historia</a>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'historias-grid',
			'dataProvider'=>$model->search(),
			'itemsCssClass'=>'table table-striped table-hover table-responsive table-condensed',
			'filter'=>$model,
			'columns'=>array(
				'id',
				'proyecto_id',
				'nombre',
				'descripcion',
				'estado_id',
				'creado',
				'modificado',
				array(
					'class'=>'CButtonColumn',
				),
			),
		)); ?>
        </div>
    </div>
</div>
