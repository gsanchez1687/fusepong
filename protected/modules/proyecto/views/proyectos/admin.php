<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Lista de Proyectos</h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('proyectocrear'); ?>" >Crear Proyecto</a>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'proyectos-grid',
			'dataProvider'=>$model->search(),
			'itemsCssClass'=>'table table-striped table-hover table-responsive table-condensed',
			'filter'=>$model,
			'columns'=>array(
				'id',
				array('name'=>'empresa_id','type'=>'raw','value'=>'$data->empresa->nombre'),
				'nombre',
				array('name'=>'estado','type'=>'raw','value'=>' $data->estado == 1 ? "Activo" : "Inactivo" '),
				array(
					'class' => 'CLinkColumn',
					'header' => 'Crear Historia',
					'label' => '<i class="icon-plus"></i>',
					'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
					'urlExpression' => 'Yii::app()->controller->createUrl("crearhistoria",array("id"=>$data->id))',
				),
				array(
					'class' => 'CLinkColumn',
					'header' => Yii::app()->params['view-text'],
					'label' => Yii::app()->params['view-icon'],
					'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['view-btn']),
					'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
				),
				array(
					'class' => 'CLinkColumn',
					'header' => Yii::app()->params['update-text'],
					'label' => Yii::app()->params['update-icon'],
					'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
					'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
				),
				array(
					'class' => 'CLinkColumn',
					'header' => Yii::app()->params['delete-text'],
					'label' => Yii::app()->params['delete-icon'],
					'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
					'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
				),
			),
		)); ?>
        </div>
    </div>
</div>

