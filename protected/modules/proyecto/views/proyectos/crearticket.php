<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Crear Proyecto</h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
            <?php $form=$this->beginWidget('CActiveForm', array('id'=>'tickets-form','enableAjaxValidation'=>false,)); ?>
            <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	        <?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-danger')); ?>
            
            <div class="form-group">
                <?php echo $form->labelEx($model,'nombre',array('class'=>'col-sm-2 control-label')); ?>
                <div class="col-sm-10">
                    <?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'nombre'); ?>
                </div>
            </div>
            <br><br>
            <div class="form-group">
                <?php echo $form->labelEx($model,'comentario',array('class'=>'col-sm-2 control-label')); ?>
                <div class="col-sm-10">
                    <?php echo $form->textField($model,'comentario',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'comentario'); ?>
                </div>
            </div>
            <br><br>

            <div class="form-group">
                <?php echo $form->labelEx($model,'estado_id',array('class'=>'col-sm-2 control-label')); ?>
                <div class="col-sm-10">
                    <?php  echo $form->dropDownList($model,'estado_id',Estados::getEstados(),array('class'=>'form-control','empty'=>'Seleccione'));  ?>
                    <?php echo $form->error($model,'estado_id'); ?>
                </div>
            </div>
            <br><br>

            <div class="buttons">
                <a href="<?php echo Yii::app()->createUrl('/proyecto/proyectos/view/id/'.$id); ?>" class="btn btn-success">Volver</a>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear Ticket' : 'Actualizar Ticket',array('class'=>'btn btn-primary')); ?>
            </div>


            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
