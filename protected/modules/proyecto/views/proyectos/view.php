<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
		<a href="<?php echo Yii::app()->createUrl('proyectolista'); ?>" class="btn btn-success">Volver</a>
            <h3 class="panel-title">Detalle Proyectos <b>#<?php echo $model->nombre; ?></b></h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'id',
				array('name'=>'empresa_id','type'=>'raw','value'=>$model->empresa->nombre),
				'nombre',
				array('name'=>'estado','type'=>'raw','value'=>$model->estado == 1 ? "Activo" : "Inactivo" ),
				'creado',
				'modificado',
			),
		)); ?>

        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Historias del proyecto <b>#<?php echo $model->nombre; ?></b></h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'historias-grid',
			'dataProvider'=>$modelHistoria->searchdetalleproyecto($model->id),
			'itemsCssClass'=>'table table-striped table-hover table-responsive table-condensed',
			'columns'=>array(
				'id',
				'nombre',
				'descripcion',
				array('name'=>'estado_id','type'=>'raw','value'=>'$data->estado->nombre' ),
				'creado',
				array(
					'class' => 'CLinkColumn',
					'header' => 'Crear Ticket',
					'label' => '<i class="icon-plus"></i>',
					'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
					'urlExpression' => 'Yii::app()->controller->createUrl("crearticket",array("id"=>$_GET["id"],"historia"=>$data->id))',
				),
			),
		)); ?>

        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Tickets </b></h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'historias-grid',
			'dataProvider'=>$modelTicket->search($model->id),
			'itemsCssClass'=>'table table-striped table-hover table-responsive table-condensed',
			'columns'=>array(
				'id',
				array('name'=>'historia_id','type'=>'raw','value'=>'$data->historia->nombre'),
				'nombre',
				'comentario',
				array('name'=>'estado_id','type'=>'raw','value'=>'$data->estado->nombre'),
				'creado',
			),
		)); ?>

        </div>
    </div>
</div>