<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'historias-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-danger')); ?>

	<br><br>
	<div class="form-group">
		<?php echo $form->labelEx($model,'nombre',array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-10">
			<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'nombre'); ?>
		</div>
	</div>
	<br><br>

	<div class="form-group">
		<?php echo $form->labelEx($model,'descripcion',array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-10">
			<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'descripcion'); ?>
		</div>
	</div>
	<br><br>

	<div class="form-group">
		<?php echo $form->labelEx($model,'estado_id',array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-10">
			<?php  echo $form->dropDownList($model,'estado_id',Estados::getEstados(),array('class'=>'form-control','empty'=>'Seleccione'));  ?>
			<?php echo $form->error($model,'estado_id'); ?>
		</div>
	</div>

	<br><br>
	<div class="buttons">
	<a href="<?php echo Yii::app()->createUrl('proyectolista'); ?>" class="btn btn-success">Volver</a>
		<?php echo CHtml::submitButton('Crear Historia',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->