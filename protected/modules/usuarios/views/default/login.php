<section class="container animated fadeInUp">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="login-wrapper">
                <header>
                    <div class="brand">
                        <a href="<?php echo Yii::app()->request->baseUrl . '/site/index' ?>" class="logo"><i class="icon-layers"></i>Control Tickets</a>
                    </div>
                </header>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Iniciar sesión</h3>
                    </div>

                    <div class="panel-body">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <?php foreach (Yii::app()->user->getFlashes() as $key => $message) : ?>
                                <?php if ($key == 'success') : ?>
                                    <div class="alert alert-info alert-dismissable"><span class="alert-close" data-dismiss="alert"></span><i class="icon-help"></i>&nbsp;&nbsp;<?php echo $message ?></div>
                                <?php else : ?>
                                    <div class="alert alert-danger alert-dismissable"><span class="alert-close" data-dismiss="alert"></span><i class="icon-ban"></i>&nbsp;&nbsp;<?php echo $message ?></div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'login-form', 'enableClientValidation' => true, 'clientOptions' => array('validateOnSubmit' => true))); ?>
                        <form class="form-horizontal" role="form">
                            
                            <div class="col-md-12">
                                <?php echo $form->textField($model, 'username', array('autocomplete' => 'off', 'class' => 'form-control input-alfanumerico-space-special', 'placeholder' => 'usuario')); ?>
                            </div>
                            
                            <br><br>

                            <div class="col-md-12">
                                <?php echo $form->passwordField($model, 'password', array('autocomplete' => 'off', 'class' => 'form-control input-alfanumerico', 'size' => '10', 'placeholder' => 'contraseña')); ?>
                            </div>

                            <br><br>

                            <div class="col-md-6">
                                <?php echo CHtml::submitButton(Yii::t('app', 'ENTRAR'), array('class' => 'btn btn-primary btn-square btn-block')); ?>
                                <hr />
                                <div style="color: #3498db" class="rememberMe">
                                    <?php echo $form->checkBox($model, 'rememberMe', array('checked' => 'checked')); ?>
                                    <?php echo $form->label($model, 'rememberMe'); ?>
                                    <?php echo $form->error($model, 'rememberMe'); ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a class="btn btn-primary btn-square btn-block" href="<?php echo Yii::app()->createUrl('/registro'); ?>">Registrar</a>
                            </div>

                        </form>

                        <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>

</section>