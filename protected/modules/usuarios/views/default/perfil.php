<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Mi Perfil</h3>
            <div class="actions pull-right">
                <i class="fa fa-expand"></i>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">
            <?php $form=$this->beginWidget('CActiveForm', array('id'=>'perfil-form','htmlOptions'=>array('enctype' => 'multipart/form-data'),'enableAjaxValidation'=>false,)); ?>
            <?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-danger')); ?>

                <div>
                    <?php echo $form->labelEx($model,'nombres'); ?>
                    <?php echo $form->textField($model,'nombres',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'nombres'); ?>
                </div>
                <br>
                <div>
                    <?php echo $form->labelEx($model,'apellidos'); ?>
                    <?php echo $form->textField($model,'apellidos',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'apellidos'); ?>
                </div>
                <br>
                
                <div class="buttons">
		            <?php echo CHtml::submitButton('Actualizar Perfil',array('class'=>'btn btn-primary')); ?>
	            </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>