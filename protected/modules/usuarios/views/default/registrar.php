<section class="container animated fadeInUp">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div id="login-wrapper">
                    <header>
                        <div class="brand">
                            <a href="index.html" class="logo">
                                <i class="icon-layers"></i>
                                <span>Control</span>Tickets</a>
                        </div>
                    </header>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                          <h3 class="panel-title">     
                           Registrarse
                        </h3>  
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <?php foreach (Yii::app()->user->getFlashes() as $key => $message) : ?>
                                <?php if ($key == 'success') : ?>
                                    <div class="alert alert-info alert-dismissable"><span class="alert-close" data-dismiss="alert"></span><i class="icon-help"></i>&nbsp;&nbsp;<?php echo $message ?></div>
                                <?php else : ?>
                                    <div class="alert alert-danger alert-dismissable"><span class="alert-close" data-dismiss="alert"></span><i class="icon-ban"></i>&nbsp;&nbsp;<?php echo $message ?></div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <p >¿Ya tienes cuenta? <a href="<?php echo Yii::app()->createUrl('/login'); ?>"><strong>Iniciar Sesion</strong></a></p>
                        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'registro-form', 'enableClientValidation' => true, 'clientOptions' => array('validateOnSubmit' => true))); ?>
                        <?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-danger')); ?>
                            
                            <div class="col-md-12">
                                <?php echo $form->labelEx($model,'empresa_id'); ?>
                                <?php echo $form->dropDownList($model,'empresa_id',Empresas::getEmpresa(),array('class'=>'form-control','empty'=>'Seleccione la empresa..')); ?>
                                <?php echo $form->error($model,'empresa_id'); ?>
                            </div>       
                        
                            <div class="col-md-12">
                                <?php echo $form->labelEx($model,'nombres'); ?>
                                <?php echo $form->textField($model, 'nombres', array('autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombres')); ?>
                                <?php echo $form->error($model,'nombres'); ?>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <?php echo $form->labelEx($model,'apellidos'); ?>
                                <?php echo $form->textField($model, 'apellidos', array('autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Apellidos')); ?>
                                <?php echo $form->error($model,'apellidos'); ?>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <?php echo $form->labelEx($model,'username'); ?>
                                <?php echo $form->textField($model, 'username', array('autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nombre de usuario')); ?>
                                <?php echo $form->error($model,'username'); ?>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <?php echo $form->labelEx($model,'password'); ?>
                                <?php echo $form->passwordField($model, 'password', array('autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Contraseña')); ?>
                                <?php echo $form->error($model,'password'); ?>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <?php echo $form->labelEx($model,'correo'); ?>
                                <?php echo $form->textField($model, 'correo', array('autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Correo Electronico')); ?>
                                <?php echo $form->error($model,'correo'); ?>
                            </div>
                            <br><br><br><br>
                            <?php echo CHtml::submitButton(Yii::t('app', 'Registrarse'), array('class' => 'btn btn-primary btn-square btn-block')); ?>
                        <?php $this->endWidget(); ?>

                    </div>
                </div>
                </div>
            </div>
        </div>

    </section>