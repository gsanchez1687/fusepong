<?php

class DefaultController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl',
			//'postOnly + delete',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('login','logout','registrar'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('perfil'),
				'users'=>array('@'),
			),
			
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionLogin() {
        Yii::app()->theme = 'login';
        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->request->baseUrl . '/site/index');
            }
        }
        $this->render('login', array('model' => $model));
    }

	//CERRAR SESION
	public function actionLogout() {
        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();
        $this->redirect(Yii::app()->request->baseUrl . '/login');
    }

	public function actionPerfil(){
		$model = Usuarios::model()->findByPK( Yii::app()->user->id );
		if(isset($_POST['Usuarios'])){
			$model->attributes = $_POST['Usuarios'];
			if($model->save())
				Yii::app()->user->setFlash('success','Se ha <b>actualizado</b> correctamente el perfil :)');
			else
				Yii::app()->user->setFlash('error','Error al <b>actualizado</b> el perfil, intente llenar todos los campos y vuelva a intentar :)');
		}

		$this->render('perfil',array('model'=>$model));
	}

	//REGISTRAR CUENTA
	public function actionRegistrar(){
		Yii::app()->theme = 'login';
		$model = new Usuarios();

		if (isset($_POST['ajax']) && $_POST['ajax'] === 'registro-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

		if (isset($_POST['Usuarios'])) {
			
			$model->attributes = $_POST['Usuarios'];
			$model->password = sha1($_POST['Usuarios']['password']);
			if( $model->save() ){
				$UsuariosEmpresas = new UsuariosEmpresas();
				$UsuariosEmpresas->usuario_id = $model->id;
				$UsuariosEmpresas->empresa_id = $_POST['Usuarios']['empresa_id'];
				
				if( $UsuariosEmpresas->save() )
					Yii::app()->user->setFlash('success','Se ha <b>registrado</b> correctamente :)');
				else
					Yii::app()->user->setFlash('error','Error al <b>registrar</b> el usuario, intente llenar todos los campos y vuelva a intentar :)');
			}else
				Yii::app()->user->setFlash('error','Error al <b>registrar</b> el usuario, intente llenar todos los campos y vuelva a intentar :)');
		}
		$this->render('registrar',array('model'=>$model));
	}
}