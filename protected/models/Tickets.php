<?php

/**
 * This is the model class for table "tickets".
 *
 * The followings are the available columns in table 'tickets':
 * @property integer $id
 * @property integer $historia_id
 * @property string $nombre
 * @property string $comentario
 * @property integer $estado_id
 * @property string $creado
 * @property string $modificado
 *
 * The followings are the available model relations:
 * @property Historias $historia
 */
class Tickets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tickets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('historia_id, nombre, comentario, estado_id', 'required'),
			array('historia_id, estado_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>100),
			array('comentario', 'length', 'max'=>300),
			array('modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
			array('creado,modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, historia_id, nombre, comentario, estado_id, creado, modificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'historia' => array(self::BELONGS_TO, 'Historias', 'historia_id'),
			'estado' => array(self::BELONGS_TO, 'Estados', 'estado_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'historia_id' => 'Historia',
			'nombre' => 'Nombre',
			'comentario' => 'Comentario',
			'estado_id' => 'Estado del Ticket',
			'creado' => 'Creado',
			'modificado' => 'Modificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.historia_id',$this->historia_id);
		$criteria->compare('t.nombre',$this->nombre,true);
		$criteria->compare('t.comentario',$this->comentario,true);
		$criteria->compare('t.estado_id',$this->estado_id);
		$criteria->compare('t.creado',$this->creado,true);
		$criteria->compare('t.modificado',$this->modificado,true);
	
		$criteria->join = 'INNER JOIN historias h ON (h.id = t.historia_id)';

		$criteria->condition = 'h.proyecto_id ='.$id;
		

		//$criteria->condition = 'e.id ='.Yii::app()->user->getState('empresa_id'); 


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tickets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
