<?php

/**
 * This is the model class for table "historias".
 *
 * The followings are the available columns in table 'historias':
 * @property integer $id
 * @property integer $proyecto_id
 * @property string $nombre
 * @property string $descripcion
 * @property integer $estado_id
 * @property string $creado
 * @property string $modificado
 *
 * The followings are the available model relations:
 * @property Estados $estado
 * @property Proyectos $proyecto
 * @property Tickets[] $tickets
 */
class Historias extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'historias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proyecto_id, nombre, descripcion, estado_id', 'required'),
			array('proyecto_id,estado_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>100),
			array('descripcion', 'length', 'max'=>300),
			array('modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
			array('creado,modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, proyecto_id, nombre, descripcion, estado_id, creado, modificado', 'safe', 'on'=>'search'),
			array('id, proyecto_id, nombre, descripcion, estado_id, creado, modificado', 'safe', 'on'=>'searchdetalleproyecto'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estado' => array(self::BELONGS_TO, 'Estados', 'estado_id'),
			'proyecto' => array(self::BELONGS_TO, 'Proyectos', 'proyecto_id'),
			'tickets' => array(self::HAS_MANY, 'Tickets', 'historia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'proyecto_id' => 'Proyecto',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'estado_id' => 'Estado Historia',
			'creado' => 'Creado',
			'modificado' => 'Modificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('proyecto_id',$this->proyecto_id);
		$criteria->compare('nombre',$this->nombre);
		$criteria->compare('descripcion',$this->descripcion);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('creado',$this->creado);
		$criteria->compare('modificado',$this->modificado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchdetalleproyecto($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.proyecto_id',$this->proyecto_id);
		$criteria->compare('t.nombre',$this->nombre);
		$criteria->compare('t.descripcion',$this->descripcion);
		$criteria->compare('t.estado_id',$this->estado_id);
		$criteria->compare('t.creado',$this->creado);
		$criteria->compare('t.modificado',$this->modificado);

		$criteria->condition = 'proyecto_id ='.$id;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Historias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
