<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property integer $id
 * @property integer $perfil_id
 * @property string $userame
 * @property string $password
 * @property string $nombres
 * @property string $apellidos
 * @property string $correo
 * @property integer $estado
 * @property string $creado
 * @property string $modificado
 *
 * The followings are the available model relations:
 * @property Empresas[] $empresases
 * @property Perfil $perfil
 */
class Usuarios extends CActiveRecord
{
	public $empresa_id;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, nombres, apellidos, correo', 'required'),
			array('empresa_id ,perfil_id, estado', 'numerical', 'integerOnly'=>true),
			array('username, password, nombres, apellidos, correo', 'length', 'max'=>100),
			array('modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
			array('creado,modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			array('estado','default','value'=>1,'setOnEmpty'=>false,'on'=>'insert'),
			array('perfil_id','default','value'=>2,'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, perfil_id, username, password, nombres, apellidos, correo, estado, creado, modificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'empresases' => array(self::HAS_MANY, 'Empresas', 'usuario_id'),
			'perfil' => array(self::BELONGS_TO, 'Perfil', 'perfil_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'perfil_id' => 'Perfil',
			'username' => 'username',
			'password' => 'Password',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'correo' => 'Correo',
			'estado' => 'Estado',
			'creado' => 'Creado',
			'modificado' => 'Modificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('perfil_id',$this->perfil_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creado',$this->creado,true);
		$criteria->compare('modificado',$this->modificado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*FUNCIONES PERSONALIZDAS*/

	function validatePassword($password) {
        return $this->hashPassword($password) === $this->password;
    }

    
    public function hashPassword($password) {
        return sha1($password);
    }

    protected function generateSalt($cost = 10) {
        if (!is_numeric($cost) || $cost < 4 || $cost > 31) {
            throw new CException(Yii::t('Cost parameter must be between 4 and 31.'));
        }
        
        $rand = '';
        for ($i = 0; $i < 8; ++$i)
            $rand .= pack('S', mt_rand(0, 0xffff));
       
        $rand .= microtime();
       
        $rand = sha1($rand, true);
        
        $salt = '$2a$' . str_pad((int) $cost, 2, '0', STR_PAD_RIGHT) . '$';
       
        $salt .= strtr(substr(base64_encode($rand), 0, 22), array('+' => '.'));
        return $salt;
    }
	/*FIN DE FUNCIONES PERSONALIZADAS*/

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
