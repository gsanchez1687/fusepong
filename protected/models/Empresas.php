<?php

/**
 * This is the model class for table "empresas".
 *
 * The followings are the available columns in table 'empresas':
 * @property integer $id
 * @property string $nombre
 * @property string $nit
 * @property string $telefono
 * @property string $direccion
 * @property string $correo
 * @property string $slug
 * @property string $url
 * @property integer $estado
 * @property string $creado
 * @property string $modificado
 *
 * The followings are the available model relations:
 * @property Usuarios $usuario
 */
class Empresas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'empresas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, nit, telefono, direccion, correo, url', 'required'),
			array('estado', 'numerical', 'integerOnly'=>true),
			array('nombre, correo, url', 'length', 'max'=>100),
			array('nit', 'length', 'max'=>20),
			array('telefono', 'length', 'max'=>15),
			array('direccion, slug', 'length', 'max'=>300),
			array('modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
			array('creado,modificado','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			array('estado','default','value'=>1,'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, nit, telefono, direccion, correo, slug, url, estado, creado, modificado', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			'SlugBehavior' => array(
				'class' => 'application.models.SlugBehavior',
				'slug_col' => 'slug',
				'title_col' => 'nombre',
				'max_slug_chars' => 300,
				'overwrite' => true
			)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuarios', 'usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'nit' => 'Nit',
			'telefono' => 'Telefono',
			'direccion' => 'Direccion',
			'correo' => 'Correo',
			'slug' => 'Slug',
			'url' => 'Url',
			'estado' => 'Estado',
			'creado' => 'Creado',
			'modificado' => 'Modificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('nit',$this->nit,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creado',$this->creado,true);
		$criteria->compare('modificado',$this->modificado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getEmpresa(){
		return CHtml::listData(self::model()->findAll('estado = 1'),'id','nombre');
	}

	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Empresas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
