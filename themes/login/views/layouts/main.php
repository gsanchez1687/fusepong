<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->

<head>
     <!--Version-->
     <?php $version = '?v=1' ?>
     <!--Lenguage Es-->
    <meta name="language" content="es">
    <!--utf-8-->
    <meta charset="utf-8">
    <!--Autor-->
    <meta name="author" content="Guillermo Enrique Sánchez Vivas">
    <!--IE-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Titulo-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <!--Color Theme movil-->
    <meta name="theme-color" content="#a749ff" />
    <!---Descripcion-->
    <meta name="description" content="">
    <!--movil-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl."/img/favicon.ico".$version; ?>" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/plugins/bootstrap/css/bootstrap.min.css".$version; ?>">
    <!-- Fonts  -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/font-awesome.min.css".$version; ?>">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/simple-line-icons.css".$version; ?>">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/animate.css".$version; ?>">
    <!-- Daterange Picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/plugins/daterangepicker/daterangepicker-bs3.css".$version; ?>">
    <!-- Calendar demo -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/clndr.css".$version; ?>">
    <!-- Switchery -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/plugins/switchery/switchery.min.css".$version; ?>">
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/main.css".$version; ?>">
    <!-- Feature detection -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/modernizr-2.6.2.min.js".$version; ?>"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/html5shiv.js".$version ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/respond.min.js".$version; ?>"></script>
    <![endif]-->
</head>

<body>
    <section class="theme-default">
    <!--main content start-->
            <section id="main-content" class="animated fadeInUp">
                    <?php echo $content; ?>
            </section>
    <!--main content end-->  
    </section>
    <!--Global JS-->
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/jquery-1.11.1.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/bootstrap/js/bootstrap.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/pace/pace.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/src/app.js".$version; ?>"></script>
    <!--Page Level JS-->
</body>

</html>
