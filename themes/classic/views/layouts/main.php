<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->

<head>
     <!--Version-->
     <?php $version = '?v=1' ?>
     <!--Lenguage Es-->
    <meta name="language" content="es">
    <!--utf-8-->
    <meta charset="utf-8">
    <!--Autor-->
    <meta name="author" content="Guillermo Enrique Sánchez Vivas">
    <!--IE-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Titulo-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <!--Color Theme movil-->
    <meta name="theme-color" content="#a749ff" />
    <!---Descripcion-->
    <meta name="description" content="">
    <!--movil-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl."/img/favicon.ico".$version; ?>" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/plugins/bootstrap/css/bootstrap.min.css".$version; ?>">
    <!-- Fonts  -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/font-awesome.min.css".$version; ?>">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/simple-line-icons.css".$version; ?>">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/animate.css".$version; ?>">
    <!-- Daterange Picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/plugins/daterangepicker/daterangepicker-bs3.css".$version; ?>">
    <!-- Calendar demo -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/clndr.css".$version; ?>">
    <!-- Switchery -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/plugins/switchery/switchery.min.css".$version; ?>">
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl."/css/main.css".$version; ?>">
    <!-- Feature detection -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/modernizr-2.6.2.min.js".$version; ?>"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/html5shiv.js".$version ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/respond.min.js".$version; ?>"></script>
    <![endif]-->
</head>

<body>
    <section id="main-wrapper" class="theme-default">
        <header id="header">
            <!--logo start-->
            <div class="brand">
                <a href="index.html" class="logo">
                    <i class="icon-layers"></i>
                    <span>Control</span>Tickets</a>
            </div>
            <!--logo end-->
            <ul class="nav navbar-nav navbar-left">
                <li class="toggle-navigation toggle-left">
                    <button class="sidebar-toggle" id="toggle-left">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
               
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown profile hidden-xs">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="meta">
                            <span class="avatar">
                                <img src="<?php echo Yii::app()->theme->baseUrl."/img/profile.png".$version; ?>" class="img-circle" alt="">
                            </span>
                        <span class="text"><?php echo Yii::app()->user->getState('nombre').' '.Yii::app()->user->getState('apellido'); ?></span>
                        <span class="caret"></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight" role="menu">
                       
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('perfil'); ?>">
                                <span class="icon"><i class="fa fa-user"></i>
                                </span>Mi Perfil</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('logout'); ?>">
                                <span class="icon"><i class="fa fa-sign-out"></i>
                                </span>Cerrar Sesion
                            </a>
                        </li>
                    </ul>
                </li>
                 <li class="toggle-fullscreen hidden-xs">
                    <button type="button" class="btn btn-default expand" id="toggle-fullscreen">
                        <i class="fa fa-expand"></i>
                    </button>
                </li>
                <li class="toggle-navigation toggle-right">
                    <button class="sidebar-toggle" id="toggle-right">
                        <i class="fa fa-indent"></i>
                    </button>
                </li>
            </ul>
        </header>
        <!--sidebar left start-->
        <aside class="sidebar sidebar-left">
            <div class="sidebar-profile">
                <div class="avatar">
                    <img class="img-circle profile-image" src="<?php echo Yii::app()->theme->baseUrl."/img/profile.png".$version; ?>" alt="profile">
                    <i class="on border-dark animated bounceIn"></i>
                </div>
                <div class="profile-body dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><h4><?php echo Yii::app()->user->getState('nombre').' '.Yii::app()->user->getState('apellido'); ?> <span class="caret"></span></h4></a>
                    <small class="title"><?php echo UsuariosEmpresas::getEmpresaUsuario(  Yii::app()->user->id ); ?></small>
                </div>
            </div>
            <nav>
                <h5 class="sidebar-header">menu</h5>
                <ul class="nav nav-pills nav-stacked">
                    <li class="active nav-dropdown">
                        <a href="#" title="empesas">
                            <i class="fa fa-fw fa-edit"></i> Empresas
                        </a>
                        <ul class="nav-sub">
                            <li><a href="<?php echo Yii::app()->createUrl('empresalista'); ?>" title="Listar empresas">Listar Empresas</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('empresacrear'); ?>" title="Crear nueva empresa">Crear Empresa</a></li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="empesas">
                            <i class="fa fa-fw fa-edit"></i> Proyectos
                        </a>
                        <ul class="nav-sub">
                            <li><a href="<?php echo Yii::app()->createUrl('proyectolista'); ?>" title="Listar empresas">Listar Proyectos</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('proyectocrear'); ?>" title="Crear nueva empresa">Crear Proyecto</a></li>
                        </ul>
                    </li>
                </ul>
            </nav> 
        </aside>
        <!--sidebar left end-->
        <!--main content start-->
        <section class="main-content-wrapper">
            <section id="main-content" class="animated fadeInUp">
                <!--Mensajes-->
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <?php foreach (Yii::app()->user->getFlashes() as $key => $message) : ?>
                        <?php if ($key == 'success') : ?>
                            <div class="alert alert-info alert-dismissable"><span class="alert-close" data-dismiss="alert"></span><i class="icon-help"></i>&nbsp;&nbsp;<?php echo $message ?></div>
                        <?php else : ?>
                            <div class="alert alert-danger alert-dismissable"><span class="alert-close" data-dismiss="alert"></span><i class="icon-ban"></i>&nbsp;&nbsp;<?php echo $message ?></div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <!--mensajes-->
                <?php echo $content; ?>
            </section>
        </section>
        <!--main content end-->
    <!--Global JS-->
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/vendor/jquery-1.11.1.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/bootstrap/js/bootstrap.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/navgoco/jquery.navgoco.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/pace/pace.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/fullscreen/jquery.fullscreen-min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/js/src/app.js".$version; ?>"></script>
    <!--Page Level JS-->
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/countTo/jquery.countTo.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/weather/js/skycons.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/daterangepicker/moment.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/daterangepicker/daterangepicker.js".$version; ?>"></script>
    <!-- ChartJS  -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/chartjs/Chart.min.js".$version; ?>"></script>
    <!-- Morris  -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/morris/js/morris.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/morris/js/raphael.2.1.0.min.js".$version; ?>"></script>
    <!-- Vector Map  -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/jvectormap/js/jquery-jvectormap-1.2.2.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/jvectormap/js/jquery-jvectormap-world-mill-en.js".$version; ?>"></script>
    <!-- Gauge  -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/gauge/gauge.min.js".$version; ?>"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/gauge/gauge-demo.js".$version; ?>"></script>
    <!-- Calendar  -->
    <script src="<?php echo Yii::app()->theme->baseUrl."".$version; ?>/plugins/calendar/clndr.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl."".$version; ?>/plugins/calendar/clndr-demo.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <!-- Switch -->
    <script src="<?php echo Yii::app()->theme->baseUrl."/plugins/switchery/switchery.min.js".$version; ?>"></script>
    <!--Load these page level functions-->
    <script>
    $(document).ready(function() {
        app.dateRangePicker();
        app.chartJs();
        app.weather();
        app.spinStart();
        app.spinStop();
    });
    </script>
</body>

</html>
